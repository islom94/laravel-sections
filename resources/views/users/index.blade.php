@extends('layouts.app')

@section('content')
    <div class="container">
        @include('messages')
        <div class="card">
            <div class="card-header">
                <span>
                    Users
                </span>
                <a class="btn btn-primary float-right" href="{{ route('users.create') }}" role="button">Add</a>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>
                                    <div class="btn-toolbar" role="toolbar">
                                        <div class="btn-group mr-2" role="group">
                                            <a class="btn btn-secondary" href="{{ route('users.edit', $user->id) }}">Edit</a>
                                        </div>
                                        <div class="btn-group" role="group" aria-label="First group">
                                            <form method="POST" action="{{ route('users.destroy', $user->id) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{ $users->links() }}
    </div>
@endsection
