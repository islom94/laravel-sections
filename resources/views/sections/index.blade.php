@extends('layouts.app')

@section('content')
    <div class="container">
        @include('messages')
        <div class="card">
            <div class="card-header">
                <span>
                    Sections
                </span>
                <a class="btn btn-primary float-right" href="{{ route('sections.create') }}" role="button">Add</a>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                    </thead>
                    <tbody>
                        @foreach ($sections as $section)
                            <tr>
                                <td>
                                    <img width="50px" height="50px" class="img-fluid" src="{{ isset($section->logo) ? Storage::url($section->logo) : '' }}">
                                </td>
                                <td>
                                    <b>{{ $section->name }}</b><br>
                                    {{ Str::limit($section->description, 75) }}
                                </td>
                                <td>
                                    @if ($section->users()->exists())
                                        <b>Users</b><br>
                                        <ol>
                                            @foreach ($section->users as $user)
                                                <li>
                                                    {{ $user->name }}
                                                </li>
                                            @endforeach
                                        </ol>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-toolbar" role="toolbar">
                                        <div class="btn-group mr-2" role="group">
                                            <a class="btn btn-secondary" href="{{ route('sections.edit', $section->id) }}">Edit</a>
                                        </div>
                                        <div class="btn-group" role="group" aria-label="First group">
                                            <form method="POST" action="{{ route('sections.destroy', $section->id) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{ $sections->links() }}
    </div>
@endsection
