@extends('layouts.app')
@push('scripts')
<script>
    window.addEventListener('load', function() {
        $('#users').selectpicker();
    });
</script>
@endpush
@section('content')
    <div class="container">
        @include('messages')
        <div class="card">
            <div class="card-header">
                <span>
                    Sections
                </span>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('sections.update', $section->id) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="name" name="name" class="form-control" id="name" value="{{ $section->name }}" placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" class="form-control" id="description" placeholder="Enter description">{{ $section->description }}</textarea>
                    </div>
                    <div class="custom-file">
                        <label for="logo" class="custom-file-label">Choose file</label>
                        <input type="file" name="logo" class="custom-file-input" id="logo">
                    </div>
                    <div class="form-group mt-3">
                        <label for="users">Users</label>
                        <select name="users[]" id="users" class="selectpicker form-control" multiple data-live-search="true">
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}" @if ($section->users->contains($user->id)) selected @endif>{{ $user->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Send</button>
                </form>
            </div>
        </div>
    </div>
@endsection
