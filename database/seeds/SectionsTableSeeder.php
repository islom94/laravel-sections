<?php

use Illuminate\Database\Seeder;
use App\Section;
use Illuminate\Support\Arr;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Section::class, 15)->create()->each(function($section) {
            $section->users()->saveMany(factory(App\User::class, mt_rand(0, 3))->make());
        });
    }
}
