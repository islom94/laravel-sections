<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!User::where('email', 'admin@test.loc')->exists()) {
            User::create([
                'name'     => 'Admin',
                'email'    => 'admin@test.loc',
                'password' => bcrypt('password'),
            ]);
        }
        factory(User::class, 15)->create();
    }
}
