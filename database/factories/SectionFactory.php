<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Section;
use App\User;
use Faker\Generator as Faker;

$factory->define(Section::class, function (Faker $faker) {
    return [
        'name'        => $faker->company,
        'description' => $faker->realText,
    ];
});
